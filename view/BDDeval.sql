-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 22 Juillet 2020 à 09:55
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP :  7.2.31-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `BDDeval`
--

-- --------------------------------------------------------

--
-- Structure de la table `TBL_article`
--

CREATE TABLE `TBL_article` (
  `ID` int(11) NOT NULL,
  `IDingredient` int(50) NOT NULL,
  `IDmois` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `TBL_article`
--

INSERT INTO `TBL_article` (`ID`, `IDingredient`, `IDmois`) VALUES
(1, 1, 1),
(14, 2, 1),
(15, 3, 1),
(16, 4, 1),
(17, 5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `TBL_fruit_legume`
--

CREATE TABLE `TBL_fruit_legume` (
  `ID` int(11) NOT NULL,
  `nom` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `TBL_fruit_legume`
--

INSERT INTO `TBL_fruit_legume` (`ID`, `nom`) VALUES
(1, 'Betterave'),
(2, 'carotte'),
(3, 'Celerie branche'),
(4, 'Chou chinois'),
(5, 'fenouil');

-- --------------------------------------------------------

--
-- Structure de la table `TBL_mois`
--

CREATE TABLE `TBL_mois` (
  `ID` int(11) NOT NULL,
  `nom` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `TBL_mois`
--

INSERT INTO `TBL_mois` (`ID`, `nom`) VALUES
(1, 'janvier'),
(2, 'février'),
(3, 'mars'),
(4, 'avril');

-- --------------------------------------------------------

--
-- Structure de la table `TBL_user`
--

CREATE TABLE `TBL_user` (
  `ID` int(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `TBL_article`
--
ALTER TABLE `TBL_article`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IDingredient` (`IDingredient`),
  ADD KEY `IDmois` (`IDmois`);

--
-- Index pour la table `TBL_fruit_legume`
--
ALTER TABLE `TBL_fruit_legume`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `TBL_mois`
--
ALTER TABLE `TBL_mois`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `TBL_user`
--
ALTER TABLE `TBL_user`
  ADD KEY `ID` (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `TBL_article`
--
ALTER TABLE `TBL_article`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `TBL_fruit_legume`
--
ALTER TABLE `TBL_fruit_legume`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `TBL_mois`
--
ALTER TABLE `TBL_mois`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `TBL_article`
--
ALTER TABLE `TBL_article`
  ADD CONSTRAINT `TBL_article_ibfk_1` FOREIGN KEY (`IDmois`) REFERENCES `TBL_mois` (`ID`),
  ADD CONSTRAINT `TBL_article_ibfk_2` FOREIGN KEY (`IDingredient`) REFERENCES `TBL_fruit_legume` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

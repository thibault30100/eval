<?php
    // On définit un login et un mot de passe de base pour tester notre exemple. Cependant, vous pouvez très bien interroger votre base de données afin de savoir si le visiteur qui se connecte est bien membre de votre site
    $login_valide = "paulette@biokop.fr";
    $pwd_valide = "AlèsDansLaPlaceOuais!";

    // on teste si nos variables sont définies
    if (isset($_POST['login']) && isset($_POST['pwd'])) {

    	// on vérifie les informations du formulaire, à savoir si le pseudo saisi est bien un pseudo autorisé, de même pour le mot de passe
    	if ($login_valide == $_POST['login'] && $pwd_valide == $_POST['pwd']) {
    		// dans ce cas, tout est ok, on peut démarrer notre session

    		// on la démarre :)
    		session_start ();
    		// on enregistre les paramètres de notre visiteur comme variables de session ($login et $pwd) (notez bien que l'on utilise pas le $ pour enregistrer ces variables)
    		$_SESSION['login'] = $_POST['login'];
    		$_SESSION['pwd'] = $_POST['pwd'];

    		// on redirige notre visiteur vers une page de notre section membre
    		header ('location: admin.php');
    	}
    	else {
            // Le visiteur n'a pas été reconnu comme étant membre de notre site. 
            header ('location: erreurlogin.php');
    		echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
    		// puis on le redirige vers la page d'accueil
    		echo '<meta http-equiv="refresh" content="0;URL=index.html">';
    	}
    }
    
    ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <link rel="stylesheet" href="login.css">
    <link rel="stylesheet" href="index.css">
</head>
<body>

<header>
    
    <h1 class="h1header">Les fruits et légumes frais BIOKOP</h1>
    </header>
    <div id="navigation">
<ul>
    <li><a href="index.php" title="aller à la section accueil">Accueil</a></li>
    <li><a href="login.php" title="aller à la section login">login</a></li>
</ul>
</div>
<br/>
<div id="container">
            <!-- zone de connexion -->
    <form action="login.php" method="post">
        <h1>Connexion</h1>
        <label><b>Nom d'utilisateur</b></label>
        <input type="text" name="login" placeholder="Entrer le nom d'utilisateur" required>
        <br />
        <label><b>Mot de passe</b></label>
        <input type="password" name="pwd" placeholder="Entrer le mot de passe" required><br />
        <input type="submit" value="Connexion">
    </form>
</body>
</html>